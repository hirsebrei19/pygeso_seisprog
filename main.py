# %% <-- imports ... douh %load_ext autoreload
# %% <-- imports ... douh %autoreload
import os
import signal

import h5py
import time
import math as m
import numpy as np
import scipy.signal
import matplotlib
import pandas as pd
from pandas import DataFrame
from matplotlib.backends.backend_agg import FigureCanvasAgg
import matplotlib.ticker as ticker
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import multiprocessing as mp
from definitions import pix2inch, changePlotSize, butter_bandpass, butter_bandpass_filter, phase_to_strain
from memory_profiler import profile
from scipy.fft import fft, fftfreq, fftshift

t_tot = time.perf_counter()

from rich.console import Console

console = Console()
console = Console(color_system="windows")
console.print('==> Program starts <==', style='bold')
console.print('======================', style='bold')
# console.print("Hello [bold][blue]World[/blue][/bold]")
## <-- define functions needed for the script
"""
Questions for AP-Sensing:
1. Was hat es mit den 1.92 m Messbeginn auf sich? Bei welchem kabelmeter ist der konkrete Beginn der hdf5 Datei?
2. HDF5-Files: Bei 1000 m Kabellange kommen 995 m nochwas heraus, wenn man points * spatial sampling nimmt????
3. Was hat es mit dem Metadateneintrag ZeroOffset aufsich?
4. Wieso der Abfall des Freqspektrums gen höheren Zahlen?

Questions for GESO
1. Skalierung der Daten automatisch auf den Maximalwert / Mittelwert oder manuell vornehmen?

Zeiten: 21-23.01.
      t1 =  24h 
      26.01. 
      start = 13:25
      end = 13:50
      start = 16:15
      27.01 = 1h 
      29.01 = 16:30 - 17:50
      01.02. = 18:00 - 1930
      
"""


class DAS_instance():

    def __init__(self, file_path):  # <-- method running on creation of the class
        self.file_path = file_path
        self.file_name = os.path.basename(self.file_path)

    # method for hdf5 reading
    def read_hdf5(self, time_start, time_end, teufen_start, teufen_end):
        t1 = time.perf_counter()
        with h5py.File(self.file_path, 'r', libver='latest') as f:
            self.repetition_frequency = f['DAQ/RepetitionFrequency'][0]
            self.spatial_sampling = f['/ProcessingServer/SpatialSampling'][0]
            self.shishashape = f['DAS'].shape
            self.trace_count = f['/Metadata/TraceCount'][0]
            self.points = f['/DAQ/SpatialPoints'][0]
            self.spatial_sampling_points = f['/ProcessingServer/SpatialSamplingPoints'][0]
            self.gauge_length = f['/ProcessingServer/GaugeLength'][0]
            self.zeroOffset = f['/Metadata/ZeroOffset'][0]

        # calculate additional metadata
        self.recTime = "not existing"
        self.time_start = time_start
        self.time_end = time_end
        self.teufen_start = teufen_start
        self.teufen_end = teufen_end
        self.length = teufen_end - teufen_start

        # calculate the measuring point number range from user input depth range [m]
        self.point_start = m.floor(teufen_start / self.spatial_sampling)
        self.point_end = m.ceil(teufen_end / self.spatial_sampling)

        # calculate the sample number from user input time interval [s]
        self.sample_start = m.floor(self.time_start * self.repetition_frequency)
        self.sample_end = m.ceil(self.time_end * self.repetition_frequency)

        with h5py.File(self.file_path, 'r', libver='latest') as f:
            dphase = f.get('DAS')[self.sample_start:self.sample_end, self.point_start:self.point_end]
            self.strain = phase_to_strain(np.cumsum(dphase, axis=0) * (np.pi / 2 ** 15), self.gauge_length)
            console.print('→ DAS-Daten eingelesen in {:.2f} s'.format(time.perf_counter() - t1), style='bold green')

    def read_iab_wav(self, time_start, time_end):
        t1 = time.perf_counter()
        self.repetition_frequency = [int(s) for s in (pd.read_table(self.file_path, decimal=',', nrows=1, header=None).to_string()).split() if s.isdigit()][2]
        self.data = pd.read_table(self.file_path, decimal=',', skiprows=[0, 1]).to_numpy()[:, 0:1]
        self.strain = np.asarray([x * ((1 / self.repetition_frequency) ** 2) for x in self.data])
        self.spatial_sampling = 0
        self.trace_count = "not existing"
        self.points = "not existing"
        self.spatial_sampling_points = "not existing"
        self.gauge_length = "not existing"
        self.zeroOffset = "not existing"
        # calculate additional metadata
        self.recTime = "not existing"
        self.time_start = time_start
        self.time_end = time_end
        self.teufen_start = 0
        self.teufen_end = "not existing"
        self.length = "not existing"

        # calculate the sample number from user input time interval [s]
        self.sample_start = m.floor(self.time_start * self.repetition_frequency)
        self.sample_end = m.ceil(self.time_end * self.repetition_frequency)
        console.print('→ Körperschall-Daten eingelesen in {:.2f} s'.format(time.perf_counter() - t1), style='bold green')

    def generate_test_case(self, length=1, sample_freq=20000, signal_freq=[1000, 1001, 1002], noise=False, noise_gain=0.1):
        t1 = time.perf_counter()
        # generate time array based on length and sample frequency
        self.repetition_frequency = sample_freq

        time_array = np.linspace(0, length, length * sample_freq, dtype=float)
        i = 0
        signal = 0
        # generate a = sin(ω * t)
        for i, freq in enumerate(signal_freq):
            if i == 0:
                signal = np.sin(2 * np.pi * freq * time_array)
            else:
                print(i)
                signal += np.sin(2 * np.pi * freq * time_array)
        signal /= signal.max()
        if noise:
            signal += np.random.normal(0, noise_gain, len(signal))
            signal /= signal.max()

        signal = np.asarray(signal).reshape(-1, 1)
        self.strain = np.asarray([x * ((1 / sample_freq) ** 2) for x in signal])
        signal_freq = [int(x / 1000) for x in signal_freq]

        if noise:
            self.file_name = f"sin_f_{str(signal_freq)[1:-1]}_kHz_sample_f_{str(sample_freq / 1000)}_kHz_noise_gain{noise_gain}"
        else:
            self.file_name = f"sin_f_{str(signal_freq)[1:-1]}_kHz_sample_f_{str(sample_freq)}_Hz"

        self.spatial_sampling = 0
        self.trace_count = "not existing"
        self.points = "not existing"
        self.spatial_sampling_points = "not existing"
        self.gauge_length = "not existing"
        self.zeroOffset = "not existing"
        # calculate additional metadata
        self.recTime = "not existing"
        self.time_start = 0
        self.time_end = length
        self.teufen_start = 0
        self.teufen_end = "not existing"
        self.length = "not existing"
        console.print('→ Testfall generiert in {:.2f} s'.format(time.perf_counter() - t1), style='bold green')

    # method to bandpass filter the strain data
    def bandpass_filtering(self, bandpass_interval):
        console.print('Achtung, der Bandpass returned nur sein Ergebnis. mach ma n dictn. oder so dfraus', style='bold')
        banana_split = bandpass_interval.split(" ", 3)
        return butter_bandpass_filter(
            self.strain,
            int(banana_split[0]),
            float(banana_split[2]) - 0.01,
            self.repetition_frequency,
            order=10
        )

    # method to save self.strain into Dataframe with columns=depth, index=seconds
    def strain_2_dataframe(self):
        self.strain_df = DataFrame(data=self.strain)
        self.strain_df.index = [(1 / self.repetition_frequency) * i + self.time_start for i in range(len(self.strain_df.index))]
        self.strain_df.columns = [round(self.spatial_sampling * i + self.teufen_start, 4) for i in range(len(self.strain_df.columns))]

    # method to plot seismogramm
    def seis_plot_large(self, bandpass_interval, scaling_factor=15):
        # matplotlib.rcParams['backend'] = 'agg'
        # matplotlib.rcParams['path.simplify'] = True
        # matplotlib.rcParams['path.snap'] = False
        # matplotlib.rcParams['agg.path.chunksize'] = 20000  # <-- 0 = 12.08s, 20000 = 13.20s
        norm = self.bandpass_filtering(bandpass_interval)
        norm /= np.linalg.norm(norm, axis=-1)[:, np.newaxis] / scaling_factor
        df = DataFrame(data=norm)
        del norm
        df.index = [(1 / self.repetition_frequency) * i + self.time_start for i in range(len(df.index))]
        df.columns = [self.spatial_sampling * i + self.teufen_start for i in range(len(df.columns))]
        df = df.add(df.columns)
        t1 = time.perf_counter()
        xres, yres, fonts = changePlotSize(9933, 65535, 1, 1)
        plt.rcParams.update({'font.size': fonts})
        plt.rcParams["figure.dpi"] = 72
        fig, ax = plt.subplots()
        df.plot(
            linewidth=2,
            fontsize=fonts,
            ax=ax,
            legend=False,
            color=['r', 'b'],
            figsize=(xres, yres),
            grid=True,
            title=self.get_file_name(),
            xlim=(min(df.index), max(df.index)),
            ylim=(min(df.columns), max(df.columns)),
            xlabel="time [s]",
            ylabel="depth [m]"
        )
        ax.axes.xaxis.set_major_locator(ticker.MaxNLocator(20))
        ax.axes.xaxis.set_minor_locator(ticker.MaxNLocator(400))
        ax.axes.yaxis.set_major_locator(ticker.MaxNLocator(200))
        ax.axes.yaxis.set_minor_locator(ticker.MaxNLocator(800))
        ax.axes.tick_params(which='both', width=4)
        ax.axes.tick_params(which='major', length=20)
        ax.tick_params(which='minor', length=7)
        fig.tight_layout(h_pad=0, w_pad=0)
        fig.gca().invert_yaxis()

        console.print('==> Dauer zum Plotten: {:.2f} s'.format(time.perf_counter() - t1), style='bold green')

        t1 = time.perf_counter()
        canvas = FigureCanvasAgg(fig)
        canvas.draw()
        rgb = Image.fromarray(np.asarray(canvas.buffer_rgba())).convert("RGB")

        console.print('==> Bildgrose des Bildbuffers je Farbkanal in px × px: {}'.format(rgb.size), style='bold green')

        rgb.save(r'data_out\{}.tiff'.format(self.file_name), format="tiff", compression="tiff_lzw")
        del df
        # plt.savefig(r'data_out\test.eps', bbox_inches='tight')
        console.print('==> Alphachannel loschen und abspeichern in {:.2f} s.'.format(time.perf_counter() - t1), style='bold green')

    # method to plot PSD plots of given dataframe
    def plot_psd(self, df_column, nfft, ymin, ymax, add_to_title):
        console.print('Achtung, hast DU strain_2_dataframe vorher ausgeführt?', style='bold')
        console.print('Du befindest dich im PSD plot.', style='bold')
        x, y, f = changePlotSize(1920, 1080, 1, 1)
        plt.rcParams.update({'font.size': 26})
        fig, axs = plt.subplots(figsize=(x, y), constrained_layout=True)
        axs.set_xlim(0, 20000 / 2)
        axs.set_ylim(ymin, ymax)
        axs.psd(self.strain_df[df_column], NFFT=nfft, noverlap=nfft / 2, Fs=self.repetition_frequency, scale_by_freq=True, lw=3)
        axs.set_title(self.file_name)
        fig.suptitle(f"depth: {df_column:.2f} m || {nfft} = nfft\n{add_to_title}")
        axs.set_xlabel('Frequency [Hz]')
        axs.set_ylabel('Power Spectral Density [dB / Hz]')
        plt.savefig(f'data_out/tmp/{self.file_name}_{df_column:.1f}_nfft{nfft}_PSD.png')
        return plt.show(), plt.figure().clear('all')

    def strain_subsample(self, sample_freq):
        console.print('Achtung, braucht Numpy Array als Input...', style='bold')
        scaling = int(self.repetition_frequency / sample_freq)
        self.repetition_frequency = sample_freq
        self.strain = self.strain[::scaling]
        self.file_name = self.file_name + "_subsampled"


# %%
def plot_test_case():
    for x in test.strain_df.columns:
        nffts = [64, 256, 1024, 2048, 4096, 16384, 65536, 262144, len(test.strain_df)]
        for nf in nffts:
            if nf == len(test.strain_df):
                test.plot_psd(x, nf, y_lim, y_max, "DFT über gesamte Länge - Generiert")
            else:
                test.plot_psd(x, nf, y_lim, y_max, "FFT in Blöcken der Länge nfft gerechnet - Generiert")


file = r"data_in/2021-12-01__14-24--7bar_io_14C.txt"
y_lim, y_max = -280, -200

test = DAS_instance(file)
test.generate_test_case(15, 120000, [1000])
test.strain_2_dataframe()
plot_test_case()
test.strain_subsample(sample_freq=20000)
test.strain_2_dataframe()
plot_test_case()

test.generate_test_case(15, 120000, [1000], noise=True, noise_gain=5)
test.strain_2_dataframe()
plot_test_case()
test.strain_subsample(sample_freq=20000)
test.strain_2_dataframe()
plot_test_case()

test.generate_test_case(15, 120000, [1000, 2000, 3000, 4000])
test.strain_2_dataframe()
plot_test_case()
test.strain_subsample(sample_freq=20000)
test.strain_2_dataframe()
plot_test_case()

test.generate_test_case(15, 120000, [10000, 11000, 12000, 13000, 14000, 15000, 16000])
test.strain_2_dataframe()
plot_test_case()
test.strain_subsample(sample_freq=20000)
test.strain_2_dataframe()
plot_test_case()

test.generate_test_case(15, 120000, [10000, 11000, 12000, 13000, 14000, 15000, 16000], noise=True, noise_gain=5)
test.strain_2_dataframe()
plot_test_case()
test.strain_subsample(sample_freq=20000)
test.strain_2_dataframe()
plot_test_case()

test.generate_test_case(15, 120000, [8000, 9000, 11000])
test.strain_2_dataframe()
plot_test_case()
test.strain_subsample(sample_freq=20000)
test.strain_2_dataframe()
plot_test_case()

test.generate_test_case(15, 120000, [18000, 19000, 21000])
test.strain_2_dataframe()
plot_test_case()
test.strain_subsample(sample_freq=20000)
test.strain_2_dataframe()
plot_test_case()

# %% read in HDF5 of IAB and Epe
folder_directory = 'data_in'
filepathlist = [os.path.join(path, name) for path, subdirs, files in os.walk(folder_directory) for name in files]
file = r"data_in/2021-12-01__14-24--7bar_io_14C.txt"

# list of bandpass intervals in given format: "{Hz_begin} - {Hz_end} Hz" e.g. "5 - 500 Hz" or "5000 - 8000 Hz"
bandpass_interval_list = ["100 - 10000 Hz", "500 - 1000 Hz", "5000 - 10000 Hz"]

das = DAS_instance(filepathlist[3])
das.read_hdf5(time_start=0, time_end=60, teufen_start=30, teufen_end=50)
das.strain_2_dataframe()

iab = DAS_instance(file)
iab.read_iab_wav(time_start=0, time_end=15)
iab.strain_2_dataframe()
