poly, sfm, lidar3d, d3scanner, scaniverse, metascan, sitescape, everypoint = [
    [3.5, 4.6],
    [2.11, 2.3],
    [12, 12],
    [3.6, 3.5],
    [3.2, 5.8],
    [3.8, 5.7],
    [2.6, 4.0],
    [4.3, 8.1]
]
# %%
list = [poly, sfm, lidar3d, d3scanner, scaniverse, metascan, sitescape, everypoint]
names = ["Polycam", "iPad (SfM)", "LiDAR Scanner 3D", "3D Scanner App", "Scaniverse", "Metascan", "SiteScape", "EveryPoint"]

for x, i in enumerate(list):
    low = i[0] - 1.96 * i[1]
    high = i[0] + 1.96 * i[1]
    gesamt = i[0] + 2 * 1.96 * i[1]
    print(names[x], "95 % CI [{:.1f}, {:.1f} // span {:.1f}]".format(low, high, gesamt))

##
import numpy as np
import matplotlib.pyplot as plt
import math

# width of the bars
barWidth = 0.7

# Choose the height of the blue bars
bars1 = [y for y, x in list]

# Choose the height of the cyan bars
bars2 = [10.8, 9.5, 4.5]

# Choose the height of the error bars (bars1)
yer1 = [x * 1.96 for y, x in list]

# Choose the height of the error bars (bars2)
yer2 = [1, 0.7, 1]

# The x position of bars
r1 = np.arange(len(bars1))
r2 = [x + barWidth for x in r1]


def pix2inch(px, dpi):
    return px / dpi


DPI = 490
# fig, ax = plt.subplots(1, 1)
plt.figure(figsize=(pix2inch(9221 / 4, DPI), pix2inch(5187 / 1.8, DPI)), dpi=DPI)
plt.grid(zorder=0, linewidth="0.8")
plt.bar(r1, bars1, width=barWidth, color='red', edgecolor='black', yerr=yer1, capsize=8, label="RMS of M3C2 distances", zorder=3)

# Create cyan bars
# plt.bar(r2, bars2, width=barWidth, color='cyan', edgecolor='black', yerr=yer2, capsize=7, label='sorgho')

# general layout
plt.xticks([r for r in range(len(bars1))], names, rotation=-90, fontsize=10)
plt.tight_layout(pad=2)
plt.ylabel('M3C2 distance [cm]')
plt.title("M3C2 distances & 95 % CI -- Teufelslöcher")
plt.legend()

# Show graphic
plt.show(dpi=300)
# %%
import multiprocessing as mp
import time

import matplotlib.pyplot as plt
import numpy as np

# Fixing random state for reproducibility
np.random.seed(19680801)


class ProcessPlotter:
    def __init__(self):
        self.x = []
        self.y = []

    def terminate(self):
        plt.close('all')

    def call_back(self):
        while self.pipe.poll():
            command = self.pipe.recv()
            if command is None:
                self.terminate()
                return False
            else:
                self.x.append(command[0])
                self.y.append(command[1])
                self.ax.plot(self.x, self.y, 'ro')
        self.fig.canvas.draw()
        return True

    def __call__(self, pipe):
        print('starting plotter...')

        self.pipe = pipe
        self.fig, self.ax = plt.subplots()
        timer = self.fig.canvas.new_timer(interval=1000)
        timer.add_callback(self.call_back)
        timer.start()

        print('...done')
        plt.show()


class NBPlot:
    def __init__(self):
        self.plot_pipe, plotter_pipe = mp.Pipe()
        self.plotter = ProcessPlotter()
        self.plot_process = mp.Process(
            target=self.plotter, args=(plotter_pipe,), daemon=True)
        self.plot_process.start()

    def plot(self, finished=False):
        send = self.plot_pipe.send
        if finished:
            send(None)
        else:
            data = np.random.random(2)
            send(data)


def main():
    pl = NBPlot()
    for ii in range(10):
        pl.plot()
        time.sleep(0.5)
    pl.plot(finished=True)


if __name__ == '__main__':
    if plt.get_backend() == "MacOSX":
        mp.set_start_method("forkserver")
    main()
