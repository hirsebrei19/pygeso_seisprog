##
globals().clear()

## <-- imports ... douh
import os
import h5py
import time
import math as m
import numpy as np
import scipy.signal
import matplotlib

from pandas import DataFrame
from matplotlib.backends.backend_agg import FigureCanvasAgg
import matplotlib.ticker as ticker
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

import memory_profiler
import threading


t_tot = time.perf_counter()
matplotlib.rcParams['backend'] = 'agg'
matplotlib.rcParams['path.simplify'] = False
matplotlib.rcParams['path.snap'] = False
# matplotlib.rcParams['agg.path.chunksize'] = 20000  # <-- 0 = 12.08s, 20000 = 13.20s

from rich.console import Console

console = Console()
console = Console(color_system="windows")
console.print('==> Program starts <==', style='bold')
console.print('======================', style='bold')
# console.print("Hello [bold][blue]World[/blue][/bold]")
## <-- define functions needed for the script
"""
Questions for AP-Sensing:
1. Was hat es mit den 1.92 m Messbeginn auf sich? Bei welchem kabelmeter ist der konkrete Beginn der hdf5 Datei?
2. HDF5-Files: Bei 1000 m Kabellange kommen 995 m nochwas heraus, wenn man points * spatial sampling nimmt????
3. Was hat es mit dem Metadateneintrag ZeroOffset aufsich?

Questions for GESO
1. Skalierung der Daten automatisch auf den Maximalwert / Mittelwert oder manuell vornehmen?

Zeiten: 21-23.01.
       24h gesamt
"""


# """ Eigene Definitionen """
def pix2inch(pixel):
    return pixel / 72


def changePlotSize(x_res, y_res, subplotCountX, subplotCountY):
    # plt.figure(figsize=(pix2inch(x_res * subplotCountX), pix2inch(y_res * subplotCountY)))
    # plt.rcParams.update({'font.size': int(m.sqrt(x_res * y_res) * 2.25e-02)})
    return pix2inch(x_res * subplotCountX), pix2inch(y_res * subplotCountY), int(m.sqrt(x_res * y_res) * 6e-03)


# functions from Ap-Sensing
def phase_to_strain(phase, guage_length):
    epsilon = 0.17
    n0 = 1.4682
    P11 = 0.12
    P12 = 0.27
    scale_factor = 1 - (0.5 * n0 * n0) * (P12 - (epsilon * (P11 + P12)))
    wave_length = 1550e-9
    strain = ((phase * wave_length) / (2 * 2 * np.pi * n0 * guage_length * scale_factor))
    return strain


# bandpass functions defined
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = scipy.signal.butter(order, [low, high], btype='band', output='sos')
    return sos


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass(lowcut, highcut, fs, order=order)
    y = scipy.signal.sosfilt(sos, data)
    return y


## <-- define class for DAS data instances

class DAS_instance(threading.Thread):
    def __init__(self, file_path):  # <-- method running on creation of the class
        threading.Thread.__init__(self)
        self.file_path = file_path

        # get metadata from file
        # Achtung hier solltest du noch shapes etc. rausloschen, da diese sich andern je nach user definiertem bereich
        with h5py.File(self.file_path, 'r', libver='latest') as f:
            self.repetition_frequency = f['DAQ/RepetitionFrequency'][0]
            self.spatial_sampling = f['/ProcessingServer/SpatialSampling'][0]
            self.shishashape = f['DAS'].shape
            self.trace_count = f['/Metadata/TraceCount'][0]
            self.points = f['/DAQ/SpatialPoints'][0]
            self.spatial_sampling_points = f['/ProcessingServer/SpatialSamplingPoints'][0]
            self.gauge_length = f['/ProcessingServer/GaugeLength'][0]
            self.zeroOffset = f['/Metadata/ZeroOffset'][0]

        # calculate additional metadata
        self.recTime = self.shishashape[0] * (1 / self.repetition_frequency)
        self.length = teufen_end - teufen_start

        # calculate the measuring point number range from user input depth range [m]
        self.point_start = m.floor(teufen_start / self.spatial_sampling)
        self.point_end = m.ceil(teufen_end / self.spatial_sampling)

        # calculate the sample number from user input time interval [s]
        self.sample_start = m.floor(time_start * self.repetition_frequency)
        self.sample_end = m.ceil(time_end * self.repetition_frequency)

    # method for reading in the actual DAS data in a given interval
    def run(self):
        with h5py.File(self.file_path, 'r', libver='latest') as d:
            if self.recTime < time_end:
                console.print('==> !!! Angeforderter Zeitbereich größer als Datei Zeitbereich !!!', style='bold red')
                console.print(f'==> Datei: {self.file_path}', style='bold')
                console.print(f'==> Maximaler Zeitbereich = {self.recTime} s', style='bold')
                exit()
            else:
                t1 = time.perf_counter()
                dphase = d.get('DAS')[self.sample_start:self.sample_end, self.point_start:self.point_end]
                self.strain = phase_to_strain(np.cumsum(dphase, axis=0) * (np.pi / 2 ** 15), self.gauge_length)
                console.print('==> DAS-Daten eingelesen in {:.2f} s'.format(time.perf_counter() - t1),
                              style='bold green')

        norm = self.strain
        norm /= np.linalg.norm(norm, axis=-1)[:, np.newaxis] / 15
        df = DataFrame(data=norm)
        df.index = [(1 / self.repetition_frequency) * i + time_start for i in range(len(df.index))]
        df.columns = [self.spatial_sampling * i + teufen_start for i in range(len(df.columns))]
        df = df.add(df.columns)
        t1 = time.perf_counter()
        xres, yres, fonts = changePlotSize(9933, 65535, 1, 1)
        plt.rcParams.update({'font.size': fonts})
        plt.rcParams["figure.dpi"] = 72
        fig, ax = plt.subplots()
        df.plot(
            linewidth=2,
            fontsize=fonts,
            ax=ax,
            legend=False,
            color=['r', 'b'],
            figsize=(xres, yres),
            grid=True,
            title=self.file_path(),
            xlim=(min(df.index), max(df.index)),
            ylim=(min(df.columns), max(df.columns)),
            xlabel="time [s]",
            ylabel="depth [m]"
        )
        ax.axes.xaxis.set_major_locator(ticker.MaxNLocator(20))
        ax.axes.xaxis.set_minor_locator(ticker.MaxNLocator(400))
        ax.axes.yaxis.set_major_locator(ticker.MaxNLocator(200))
        ax.axes.yaxis.set_minor_locator(ticker.MaxNLocator(800))
        ax.axes.tick_params(which='both', width=4)
        ax.axes.tick_params(which='major', length=20)
        ax.tick_params(which='minor', length=7)
        fig.tight_layout(h_pad=0, w_pad=0)
        fig.gca().invert_yaxis()

        console.print('==> Dauer zum Plotten: {:.2f} s'.format(time.perf_counter() - t1), style='bold green')

        t1 = time.perf_counter()
        canvas = FigureCanvasAgg(fig)
        canvas.draw()
        rgb = Image.fromarray(np.asarray(canvas.buffer_rgba())).convert("RGB")

        console.print('==> Bildgrose des Bildbuffers je Farbkanal in px × px: {}'.format(rgb.size), style='bold green')

        rgb.save(r'data_out\{}.tiff'.format(self.file_path()), format="tiff", compression="tiff_lzw")
        # plt.savefig(r'data_out\test.eps', bbox_inches='tight')
        console.print('==> Alphachannel loschen und abspeichern in {:.2f} s.'.format(time.perf_counter() - t1), style='bold green')


## <-- Block with user inputs

folder_directory = 'data_in'

# time [s] and depth [m] range input
time_start = 1
time_end = 10
teufen_start = 0
teufen_end = 1000

# list of bandpass intervals in given format: "{Hz_begin} - {Hz_end} Hz" e.g. "5 - 500 Hz" or "5000 - 8000 Hz"
bandpass_interval_list = ["100 - 10000 Hz", "500 - 1000 Hz", "5000 - 10000 Hz"]

## <-- Programm doing its inputs

# get file paths of every subdirectory in folder_directory (data_in) // here should go an try except blog in the final prog
filepathlist = [os.path.join(path, name) for path, subdirs, files in os.walk(folder_directory) for name in files]

## <-- Testbench for random stuff
import concurrent.futures



# def tuttifrutti(file, id):
#     new = DAS_instance(file)
#     new.read_das_data()
#     new.seis_plot_large(bandpass_interval_list[0])
#     return "readyy"
#
#
if __name__ == "__main__":

    t0 = DAS_instance(filepathlist[0])
    t1 = DAS_instance(filepathlist[1])

    t0.start()
    t1.start()

    t0.join()
    t1.join()

    # jobs = []
    # for i in range(0, len(filepathlist)):
    #     out_list = list()
    #     thread = threading.Thread(target=tuttifrutti, args=(filepathlist[i], i))
    #     jobs.append(thread)
    #
    # # Start the threads (i.e. calculate the random number lists)
    # for j in jobs:
    #     j.start()
    #
    # # Ensure all of the threads have finished
    # for j in jobs:
    #     j.join()



# for file in filepathlist:
#     myThread = threading.Thread(target=DAS_instance(file))
#     myThread.start()
#     print("The thread's ID is : " + str(myThread.ident))
#     new = DAS_instance(file)
#     new.read_das_data()
#     # new.bandpass_filtering(bandpass_interval_list[1])
#     new.seis_plot_large(bandpass_interval_list[0])

#


# names = ['a', 'b']
# with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
#     results = executor.map(tuttifrutti, filepathlist)
# tuttifrutti(filepathlist[0])
# print(list(results))
##
console.print('==> Zeit zum Ausfuhren des gesamten Skriptes {:.2f} s.'.format(time.perf_counter() - t_tot), style='bold green')