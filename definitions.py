import math as m
import scipy.signal
import numpy as np
from rich.console import Console

console = Console()
console = Console(color_system="windows")


# class Test():
#     def __init__(self):
#         self.var1 = 1
#         self.var2 = 2
#
#     def addthree(self):
#         self.var3 = self.var1 + self.var2
#
#     def addto(self):
#         self.var3 = self.var1 - self.var2
#         # return self.var
#
#
# a = Test()
# print(a.var1, a.var2)
# a.addthree()
# print(a.var1, a.var2, a.var3)
# a.addto()
# print(a.var1, a.var2, a.var3)


def phase_to_strain(phase, guage_length):
    epsilon = 0.17
    n0 = 1.4682
    P11 = 0.12
    P12 = 0.27
    scale_factor = 1 - (0.5 * n0 * n0) * (P12 - (epsilon * (P11 + P12)))
    wave_length = 1550e-9
    strain = ((phase * wave_length) / (2 * 2 * np.pi * n0 * guage_length * scale_factor))
    return strain


# bandpass functions defined
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = scipy.signal.butter(order, [low, high], btype='band', output='sos')
    return sos


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass(lowcut, highcut, fs, order=order)
    y = scipy.signal.sosfilt(sos, data)
    return y


# """ Eigene Definitionen """
def pix2inch(pixel):
    return pixel / 72


def changePlotSize(x_res, y_res, subplotCountX, subplotCountY):
    # plt.figure(figsize=(pix2inch(x_res * subplotCountX), pix2inch(y_res * subplotCountY)))
    # plt.rcParams.update({'font.size': int(m.sqrt(x_res * y_res) * 2.25e-02)})
    # hint: x_size , y_size, fontsize
    return pix2inch(x_res * subplotCountX), pix2inch(y_res * subplotCountY), int(m.sqrt(x_res * y_res) * 6e-03)
